# README #

Author: Delaney Carleton
Email: dcarlet2@uoregon.edu

The objectives of this mini-project are:
  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)
